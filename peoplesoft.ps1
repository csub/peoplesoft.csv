function Setup-Connections {
    #Connect to Office 365
    Import-Module MSOnline
    try {
        Get-Content credentials.txt | Foreach-Object{
            $var = $_.Split('=').trim()
            New-Variable -Name $var[0] -Value $var[1] 
        }
    } catch {
        Write-Error "Unable to retrieve credentials from file" -ErrorAction Stop

        exit 1
    }

    $password = ConvertTo-SecureString $powerPass -AsPlainText -Force
    $adminCredential = New-Object System.Management.Automation.PSCredential -argumentlist $powerUser,$password

    $O365Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell -Credential $adminCredential -Authentication Basic -AllowRedirection

    Import-PSSession $O365Session
    Connect-MsolService -Credential $adminCredential
 
    #AD cmdlets
    Import-Module ActiveDirectory
}

function Write-Log ($message, $level = "Information" )
{
    Write-EventLog -LogName Application -Source "peoplesoft-csv" -EntryType $level -EventId 1337 -Message $message
}

Write-Log -message "state=started"

Write-Log -message "state=Connecting-o365"

try {
    Setup-Connections
} catch { 
    Write-Log -message "state=failed Failed to connect to o365" -level "Error"
    exit
}

Write-Log -message "state=Getting-o365"
$365users = Get-MsolUser -All -Synchronized | select userprincipalname,licenses,proxyaddresses

if($365users.count -eq 0) {
    Write-Log -message "state=failed Failed to get users from 0365" -level "Error"
    exit
}

$errorThreshhold = 100
$peoplesoftCSV = "peoplesoft-new.csv"
$tempFile = "tempFile.csv"
[regex]$employeeMatch ='.*(staff|fac|retire|aux|consult|future|ext_instructor|temp_agency).*'
$employeeFlag = $false

Write-Log -message "state=Getting-AD-Info"
try {
    #Sanity Check get 
    $ADCount = (Get-ADGroup -Filter 'name -eq "Staff"' -Properties members).members.count
    $ADCount += (Get-ADGroup -Filter 'name -eq "student_enrolled_o365"' -Properties members).members.count
    $ADCount += (Get-ADGroup -Filter 'name -eq "student_ite"' -Properties members).members.count
    $ADCount += (Get-ADGroup -Filter 'name -eq "faculty"' -Properties members).members.count
    $ADCount += (Get-ADGroup -Filter 'name -eq "adhoc_o365"' -Properties members).members.count
    $ADCount += (Get-ADGroup -Filter 'name -eq "O365 ORG"' -Properties members).members.count
} catch { 
    Write-Log -message "state=failed Failed unable to query Active Directory"
}

if([Math]::Abs($365users.count - $ADCount) -gt $errorThreshhold) { 
    Write-Log -message "state=failed Failed Deviation between o365user Count - Ad Group Membership Count exceeds error threshold"
    exit
}


#get count
$count = 0
Remove-Item $tempFile -ErrorAction SilentlyContinue
Write-Log -message "state=Processing-File"
foreach($user in $365users) {

    $employeeFlag = $false
    $upn = $user.userPrincipalName
    $email = ($user.proxyaddresses -cmatch '^SMTP:').split(":")[1]

    if($upn -match '^org-.*') { continue }
    
    $adinfo = get-aduser -Filter 'userprincipalname -eq $upn' -Properties memberof,employeeNumber

    if($employeeMatch.Matches($adinfo.MemberOf).Success) {  $employeeFlag = $true; }

    if($employeeFlag -and $adinfo.employeeNumber) {
        $count += 2
        try{
            $adinfo.EmployeeNumber,"BUSN",$email -join "|" | Out-File -Encoding "ASCII" -FilePath $tempFile -Append
            $adinfo.EmployeeNumber,"OCMP",$email -join "|" | Out-File -Encoding "ASCII" -FilePath $tempFile -Append
        } catch {
            Write-Log "state=failed Failed to write out $email"
            exit
        }
        

    }
    elseif($adinfo.employeeNumber) {
        $count += 1
        try {
            $adinfo.EmployeeNumber,"OCMP",$email -join "|" | Out-File -Encoding "ASCII" -FilePath $tempFile -Append 
        } catch {
            Write-Log "state=failed Failed to write out $email"
            exit
        }
        
    }
}

if($count -eq (Get-Content $tempFile | Measure-Object -Line).Lines -and $count -gt $365users.count) {
    Copy-Item $tempFile -Destination $peoplesoftCSV -Force
    Write-Log -message "state=success DONE"
}
else {
    Write-Log -message "state=failed Unable to verify counts in final file. Count != Line Count OR Count !> 365 users"
}