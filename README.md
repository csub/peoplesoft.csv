# README #

This repo generates the peoplesoft.csv file.

### How do I get set up? ###

Password must be supplied in credentials file. Credentials file must be located in same directory as peoplesoft.ps1 script.

### Who do I talk to? ###

* Questions about the repo can be directed to Eduardo Figueroa
* Questions about the utility of the repo can be directed to Brian Chen
